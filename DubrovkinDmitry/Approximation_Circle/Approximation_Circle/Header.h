#pragma once


#include <conio.h>
#include <locale>

#include <math.h>

#include <afxtempl.h>

#include <fstream>
#include <iostream>
//#include <vector>


using namespace std;

/// <summary>
/// �����. Ÿ ����������.
/// </summary>
struct Point
{
	double x = 0, y = 0;
};


///////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// <summary>
/// ���������� ���������� ����� ����� �������
/// </summary>
/// <param name="point1">������ ����� (���������)</param>
/// <param name="point2">������ ����� (���������)</param>
double DistanceBetweenTwoPoints(Point *point1, Point *point2)
{
	return (double)sqrt(
		(point1->x - point2->x)*(point1->x - point2->x) +
		(point1->y - point2->y)*(point1->y - point2->y));
}

/// <summary>
/// �������
/// </summary>
/// <param name="p">first Point</param>
/// <param name="center">second Point</param>
/// <param name="R">radius</param>
double function(Point *p, Point *center, double R)			// (Ri - R)^2
{
	return pow(sqrt(pow(p->x - center->x, 2) + pow(p->y - center->y, 2)) - R, 2);
}

/// <summary>
/// ���� �� ������� � �����
/// </summary>
/// <param name="center">Point of Center</param>
/// <param name="points">Circle's Points</param>
/// <param name="R">Radius</param>
double LoopFunction(Point *center, CArray <Point, Point> *points, double R)
{
	double sum = 0;
	for (int i = 0; i < points->GetSize(); i++)
		sum += function(&points->operator[](i), center, R);
	return sum;
}