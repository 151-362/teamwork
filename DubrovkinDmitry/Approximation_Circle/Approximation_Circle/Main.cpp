
#include "Header.h"

void ChangeValue(Point *center,  CArray <Point, Point> *points, double *radius, double accuracy, double *value)
{
	double step = 1;
	bool negativeDirection = false;

	double deviationSum, deviationSumOld;


	deviationSum = LoopFunction(center, points, *radius);

	do
	{
		deviationSumOld = deviationSum;
		*value += (1 + (-2) * (int)negativeDirection) * step;

		deviationSum = LoopFunction(center, points, *radius);

		if (deviationSum > deviationSumOld)
		{
			negativeDirection = !negativeDirection;
			step /= 2;
		}

	} 
	while (fabs(deviationSumOld - deviationSum)  > accuracy);


	///////		Changing Radius
	if (*value != *radius)
		ChangeValue(center, points, radius, accuracy, radius);
}

int main()
{
	cout.setf(ios::fixed);
	cout.precision(10);


	ifstream file;
	file.open("dataCircle.txt");

	if (!file.is_open())
	{
		cout << "Problem with opening file \"data.txt\"...";
		_getch();
		return 1;
	}

	CArray <Point, Point> points;
	Point point;
	double radius = 0;

	double sumX = 0, sumY = 0;

	while (!file.eof())					// Reading File "data.txt"
	{
		file >> point.x >> point.y;
		points.Add(point);

		sumX += point.x;
		sumY += point.y;

	}

	point.x /= points.GetSize();
	point.y /= points.GetSize();


	for (int i = 0; i < points.GetSize(); i++)		// Finding model radius
	{
		radius += DistanceBetweenTwoPoints(&point, &points[i]);
	}
	radius /= points.GetSize();


	cout << "Center Before: \t" << point.x << "\t\t" << point.y << endl;
	cout << "Radius Before: \t" << radius << endl << endl;

	///////////		Start Approximation

	double accuracy = 0.0000000001;

	double	deviationSum = 0,
		deviationSumOld = 0,
		globalDeviation = 0,
		globalDeviationOld = 0,
		step = 1;		// For example

	bool negativeDirection = false;				// positive or negative direction 


	globalDeviation = LoopFunction(&point, &points, radius);

	do {
		globalDeviationOld = globalDeviation;


		ChangeValue(&point, &points, &radius, accuracy, &point.x);
		ChangeValue(&point, &points, &radius, accuracy, &point.y);

		///////

		globalDeviation = LoopFunction(&point, &points, radius);

	} while (fabs(globalDeviation - globalDeviationOld) > accuracy);



	cout << "Center After: \t" << point.x << "\t\t" << point.y << endl;
	cout << "Radius After: \t" << radius << endl;


	_getch();
	return 0;
}