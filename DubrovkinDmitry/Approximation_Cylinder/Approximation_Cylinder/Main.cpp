
#include "Header.h"

void ChangeValue(Vector *vector, CArray <Point, Point> *points, double *radius, double accuracy, double *value)
{
	double step = 1;
	bool negativeDirection = false;

	double deviationSum, deviationSumOld;


	deviationSum = LoopFunction(vector, points, *radius);

	do
	{
		deviationSumOld = deviationSum;
		*value += (1 + (-2) * (int)negativeDirection) * step;

		VectorNormalization(vector);

		deviationSum = LoopFunction(vector, points, *radius);

		if (deviationSum > deviationSumOld)
		{
			negativeDirection = !negativeDirection;
			step /= 2;
		}

	} while (fabs(deviationSumOld - deviationSum)  > accuracy);


	///////		Changing Radius
	if (*value != *radius)
		ChangeValue(vector, points, radius, accuracy, radius);
}

int main()
{
	cout.setf(ios::fixed);
	cout.precision(20);


	ifstream file;
	file.open("dataCylinder.txt");

	if (!file.is_open())
	{
		cout << "Problem with opening file \"data.txt\"...";
		_getch();
		return 1;
	}

	CArray <Point, Point> points;
	Point point;

	double sumX = 0, sumY = 0, sumZ = 0;

	while (!file.eof())					// Reading File "data.txt"
	{
		file >> point.x >> point.y >> point.z;
		points.Add(point);

		sumX += point.x;
		sumY += point.y;
		sumZ += point.z;
	}

	point.x /= points.GetSize();
	point.y /= points.GetSize();
	point.z /= points.GetSize();


	Plane plane;
	PlaneByThreePoints(&plane, &points[0], &points[1], &points[2]);		// Plane coefficients

	Vector vector;
	vector.x = plane.A;
	vector.y = plane.B;
	vector.z = plane.C;
	vector.point.x = point.x;
	vector.point.y = point.y;
	vector.point.z = point.z;
	VectorNormalization(&vector);			// Vector Normalization

	Point pointProjection;
	double radius = 0;

	for (int i = 0; i < points.GetSize(); i++)		// Finding model radius
	{
		pointProjection = PointProjectionToLine(&points[i], &vector);

		radius += DistanceBetweenTwoPoints(&points[i], &pointProjection);
	}
	radius /= points.GetSize();

	cout << "\Vector Before: \t" << vector.x << "\t" << vector.y << "\t" << vector.z << endl;
	cout << "Center Before: \t" << vector.point.x << "\t" << vector.point.y << "\t" << vector.point.z << endl;
	cout << "Radius Before: \t" << radius << endl << endl;

	///////////		Start Approximation

	double accuracy = 0, accuracyGlobal = 0;
	//cout << "Accuracy = ";
	//cin >> accuracy;
	accuracy = 0.00000000001;
	accuracyGlobal = 0.0000000000001;

	double	deviationSum = 0,
		deviationSumOld = 0,
		globalDeviation = 0,
		globalDeviationOld = 0,
		step = 1;		// For example

	bool negativeDirection = false;				// positive or negative direction 


	globalDeviation = LoopFunction(&vector, &points, radius);

	do {
		globalDeviationOld = globalDeviation;

		////////////	Vector

		ChangeValue(&vector, &points, &radius, accuracy, &vector.x);	// Changing X - vector
		ChangeValue(&vector, &points, &radius, accuracy, &vector.y);	// Changing Y - vector
		ChangeValue(&vector, &points, &radius, accuracy, &vector.z);	// Changing Z - vector


		ChangeValue(&vector, &points, &radius, accuracy, &vector.point.x);	// Changing X - center
		ChangeValue(&vector, &points, &radius, accuracy, &vector.point.y);	// Changing Y - center
		ChangeValue(&vector, &points, &radius, accuracy, &vector.point.z);	// Changing Z - center

			

		///////

		globalDeviation = LoopFunction(&vector, &points, radius);

		cout << sqrt(pow(globalDeviation - globalDeviationOld, 2)) << endl;

	} while (fabs(globalDeviation - globalDeviationOld) > accuracyGlobal);


	cout << "\Vector After: \t" << vector.x << "\t" << vector.y << "\t" << vector.z << endl;
	cout << "Center After: \t" << vector.point.x << "\t" << vector.point.y << "\t" << vector.point.z << endl;
	cout << "Radius After: \t" << radius << endl;


	_getch();
	return 0;
}