#pragma once


#include <conio.h>
#include <locale>

#include <math.h>

#include <afxtempl.h>

#include <fstream>
#include <iostream>
//#include <vector>


using namespace std;


//////////////////////////////////////////////////////////////////////////////////////////////////		Struct

/// <summary>
/// �����. Ÿ ����������.
/// </summary>
struct Point
{
	double x = 0, y = 0, z = 0;
};

/// <summary>
/// Vector. ��� ����������. � ����� �� ���
/// </summary>
struct Vector
{
	double x = 0, y = 0, z = 0;
	Point point;
};

/// <summary>
/// ���������. Ÿ ������������. Plane Equation:	Ax + By + Cz + D = 0
/// </summary>
struct Plane
{
	float A = 0, B = 0, C = 0, D = 0;
};

//////////////////////////////////////////////////////////////////////////////////////////////////		Head

Point PointProjectionToLine(Point *point, Vector *vector);

//////////////////////////////////////////////////////////////////////////////////////////////////

/// <summary>
/// ���������� ���������� ����� ����� �������
/// </summary>
/// <param name="point1">������ ����� (���������)</param>
/// <param name="point2">������ ����� (���������)</param>
double DistanceBetweenTwoPoints(Point *point1, Point *point2)
{
	return (double)sqrt(
		(point1->x - point2->x)*(point1->x - point2->x) +
		(point1->y - point2->y)*(point1->y - point2->y) +
		(point1->z - point2->z)*(point1->z - point2->z)
	);
}

/// <summary>
/// �������
/// </summary>
/// <param name="p1">first Point</param>
/// <param name="p2">second Point</param>
/// <param name="R">radius</param>
double function(Vector *vector, Point *point, double R)			// (Ri - R)^2
{
	return pow(
		DistanceBetweenTwoPoints(
			&PointProjectionToLine(point, vector), point) - R, 2);
}

/// <summary>
/// ���� �� ������� � �����
/// </summary>
/// <param name="vector">Circle's exis vector</param>
/// <param name="points">Circle's Points</param>
/// <param name="R">Radius</param>
double LoopFunction(Vector *vector, CArray <Point, Point> *points, double R)
{
	double sum = 0;
	for (int i = 0; i < points->GetSize(); i++)
		sum += function(vector, &points->operator[](i), R);
	return sum;
}



//////////////////////////////////////////////////////////////////////////////////////////////////		// Point

/// <summary>
/// ������������� ����� �� ������ (���������� ��������������� �����)
/// </summary>
/// <param name="point">��������� �� �����</param>
/// <param name="vector">��������� �� ������</param>
Point PointProjectionToLine(Point *point, Vector *vector)
{
	Plane planeWork;

	planeWork.A = vector->x;
	planeWork.B = vector->y;
	planeWork.C = vector->z;
	planeWork.D = vector->x*(-1)*(point->x) + vector->y*(-1)*(point->y) + vector->z*(-1)*(point->z);

	double lambda;

	lambda = (-1)*(planeWork.A*vector->point.x + planeWork.B*vector->point.y + planeWork.C*vector->point.z + planeWork.D) /
		(planeWork.A*vector->x + planeWork.B*vector->y + planeWork.C*vector->z);

	Point pointProjection;

	pointProjection.x = vector->point.x + vector->x*lambda;
	pointProjection.y = vector->point.y + vector->y*lambda;
	pointProjection.z = vector->point.z + vector->z*lambda;

	return pointProjection;
}

//////////////////////////////////////////////////////////////////////////////////////////////////		// Vector

/// <summary>
/// ������������ �������.
/// </summary>
/// <param name="vector">��������� �� ������. ��������������� "a", "b", "c"</param>
void VectorNormalization(Vector *vector)
{
	vector->x /= sqrt(pow(vector->x, 2) + pow(vector->y, 2) + pow(vector->z, 2));
	vector->y /= sqrt(pow(vector->x, 2) + pow(vector->y, 2) + pow(vector->z, 2));
	vector->z /= sqrt(pow(vector->x, 2) + pow(vector->y, 2) + pow(vector->z, 2));
}

//////////////////////////////////////////////////////////////////////////////////////////////////		// Plane

/// <summary>
/// Ax+By+Cz+D=0 - ����� ������������� ��������� �� ��� ������
/// </summary>
/// <param name="plane">��������� �� ���������</param>
/// <param name="points">������ �����</param>
void PlaneByThreePoints(Plane *plane, Point *point1, Point *point2, Point *point3)
{
	plane->A = point1->y*(point2->z - point3->z) +
		point2->y*(point3->z - point1->z) +
		point3->y*(point1->z - point2->z);

	plane->B = point1->z*(point2->x - point3->x) +
		point2->z*(point3->x - point1->x) +
		point3->z*(point1->x - point2->x);

	plane->C = point1->x*(point2->y - point3->y) +
		point2->x*(point3->y - point1->y) +
		point3->x*(point1->y - point2->y);

	plane->D = (-1)*(
		point1->x*(point2->y*point3->z - point3->y*point2->z) +
		point2->x*(point3->y*point1->z - point1->y*point3->z) +
		point3->x*(point1->y*point2->z - point2->y*point1->z)
		);
}
